package socialApp;

import java.time.LocalDateTime;

/**
 * This is the implementation main class to run userCollections
 * this returns usersList,sendMessages and receive Messages
 * @author akarri1
 *
 */
public class Main {
	public static void main(String[] args) {
		
		UserCollections usercollect = new UserCollections();
		/**
		 * users creation method calling and pass the name, password and phoneNumber
		 */
		System.out.println("-----USERS LIST------");
		usercollect.createNewUser("adilakshmi",  "8675644565",   "sed@123", "read");
		usercollect.createNewUser("adilakshmi1", "9435523395",   "sdfsr@23", "read");
		usercollect.createNewUser("lakshmi",     "9435513395",   "sdsr@123","unread");
		
	    System.out.println("----SEND MESSAGES LIST----");
	    usercollect.sendMessage("8675644565", "9435523395", "hiii",  LocalDateTime.now());
        usercollect.sendMessage("8675644565", "7865655454", "hello", LocalDateTime.now());
        
        System.out.println("----RECEIVE MESSAGES LIST----");
        usercollect.receiveMessage("9545345438", "8675644565", "hii",      LocalDateTime.now());
        usercollect.receiveMessage("9545345438", "8675644565", "hello,hi", LocalDateTime.now());
        usercollect.receiveMessage("9545345438", "8435523395", "hh",       LocalDateTime.now());
        
        }
}



