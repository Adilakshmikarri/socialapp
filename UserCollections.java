package socialApp;

import java.time.LocalDateTime;
import java.util.ArrayList;


public class UserCollections {
	
         ArrayList<User> userList;
         ArrayList<Message> sendMessagesList;
         ArrayList<Message> receiveMessageList;
        /**
         * this is userCollections class to create user method with name,phone and password
         * parameters this returns user  name,phone and password.
         * @param name for userName
         * @param phone for phoneNumber
         * @param password for password
         * @return
         */
            public boolean createNewUser(String name, String phone, String password, String status) {
            	try {
            		User addUser  =  new User(name, phone, password, status);
            		userList.add(addUser);
            		return true;
				   
            	}catch(Exception a) {
            					return false;
                }
			
			
		}
		
         /**
		 * this is method to send message using senderPhone,receiverPhone and messageBody
		 * @param senderPhone
		 * @param receiverPhone
		 * @param message
		 */
		
		
		@SuppressWarnings("unlikely-arg-type")
		public boolean sendMessage(String senderPhone, String receiverPhone, String message, LocalDateTime timestamp) {
			 try {
				 
		          Message addsendMessages = new Message(senderPhone, receiverPhone, message, timestamp);
		           sendMessagesList.add(addsendMessages);
		            return true;
		     }catch(Exception a) {
				      return false;
				  
				 }
			
			}
		/**
		 * this is receiveMessage method to receiving messages
		 * @param senderPhone
		 * @param receiverPhone
		 * @param message
		 * @return 
		 */
		
		public boolean receiveMessage(String senderPhone, String receiverPhone, String message,LocalDateTime timestamp) { 
			 try {
				 Message addreceivemessage = new Message(senderPhone, receiverPhone, message, timestamp);
				 receiveMessageList.add(addreceivemessage);
				 return true;
		     }catch(Exception a) {
				          return false;
		     			}
			    }
	
		}
	         


