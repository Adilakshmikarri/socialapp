package socialApp;

import java.time.LocalDateTime;

/**
 * This is Message class to call sendMessages and receiveMessages
 * by using senderPhoneNumber and receiverPhoneNumber send Messages
 * and it returns senderPhoneNumber and receiverPhoneNumber and message
 */
import java.util.ArrayList;

public class Message {
	  String senderPhone;
	  String receiverPhone;
	  String message;
	  ArrayList<String> sendMessages;
	  ArrayList<String> receivedMessages;
	  LocalDateTime timestamp;
	  /**
	   * constructor to Messages class
	   * @param senderPhone
	   * @param receiverPhone
	   * @param message
	   * @param timestamp
	   * @throws Exception
	   */
	  Message(String senderPhone, String receiverPhone, String message, LocalDateTime timestamp) throws Exception{
	      
		  
		  if(senderPhone.trim().length() != 10) {
			   throw new Exception("number must be 10 numbers"); 
		   }
		   else if(receiverPhone.trim().length()!= 10) {
			   throw new Exception("number must be 10 numbers");
		   }else {
			      this.senderPhone   = senderPhone;
			      this.receiverPhone = receiverPhone;
			      this.message       = message;
			      this.timestamp     = LocalDateTime.now();
			      System.out.println(this);
			   
		 
		   }
	   }
	  public String toString() {
		  return   "senderPhone   :"          +senderPhone+        "\n"+
	               "receiverPhone :"          +receiverPhone+      "\n"+
				   "message       :"          +message+            "\n"+
	               "timestamp     :"          +timestamp+          "\n";
	  }
}
		  
	  
	 
	  


	



