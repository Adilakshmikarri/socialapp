package socialApp;

/**
 * This is the implementation of the User Class
 * @author akarri1
 *
 */
   public class User {
	   		String name;				// Represents the User Name
	   		String phone;				// Represents the User PhoneNumber
	   		String password;	        // Represents the User password
	   		String status;
	   		/**
	   		 * this is constructor to User class
	   		 * @param name
	   		 * @param phone
	   		 * @param password
	   		 * @throws Exception
	   		 */
	 User(String name, String phone, String password, String status)throws Exception{
		   		if(name.trim().length()<5) {
		   			throw new Exception("name is too short"); 
		   		}
		   		else if(phone.trim().length()!=10) {
		   			throw new Exception("number must be 10 numbers");
		   		}
		   		else if(password.trim().length()<4) {
		   			throw new Exception("password is not strong");
		   		}else {
		   			this.name     = name;
		   			this.phone    = phone;
		   			this.password = password;
		   			this.status   = status;
		   			System.out.println(this);
		   		}
		 
	   		}
	  
	public String toString() {
		   return      "name    :"                +name+             "\n"+
		               "phone   :"                +phone+            "\n"+
				       "password:"                +password+         "\n"+
		               "status  :"                +status+           "\n";
		               
	   }
	  
	   

}
   

